class LoanTransaction < ApplicationRecord

	belongs_to :user
	has_many :loan_transaction_details
	accepts_nested_attributes_for :loan_transaction_details

	scope :filter_search, -> (search = nil) {joins(:user, {loan_transaction_details: :book}).where("users.name like ? OR books.title like ?", "%#{search}%", "%#{search}%") if search.present?}
end

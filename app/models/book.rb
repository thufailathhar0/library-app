class Book < ApplicationRecord

	has_many :load_transaction_details

	validates :title, presence: true
	validates :description, presence: true
	validates :category, presence: true

	enum category: {komik: 1, novel: 2, biografi: 3}
	scope :filter_search, -> (search = nil) {where("title like ? OR description like ?", "%#{search}%", "%#{search}%") if search.present?}

end

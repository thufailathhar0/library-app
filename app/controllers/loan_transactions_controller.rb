class LoanTransactionsController < ApplicationController
	before_action :define_loan_transaction, only: [:show]

	def index
		@loan_transactions = LoanTransaction.filter_search(params[:search]).order(created_at: :desc).uniq
	end

	def new
		@loan_transaction = LoanTransaction.new
		@user = User.all
		@book = Book.all
	end

	def create
		@loan_transaction = LoanTransaction.new(params_loan_transaction)
		if @loan_transaction.save
			redirect_to loan_transactions_path
		else
			redirect_back new_loan_transaction_path
		end
	end

	def show
		
	end

	private

	def params_loan_transaction
		params.fetch(:loan_transaction, {}).permit(:due_date, :user_id, loan_transaction_details_attributes: [:book_id])
	end

	def define_loan_transaction
		@loan_transaction = LoanTransaction.find(params[:id])
	end
end

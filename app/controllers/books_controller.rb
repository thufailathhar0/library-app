class BooksController < ApplicationController
	before_action :define_book, only: [:edit, :update]

	def index
		@books = Book.filter_search(params[:search]).order(created_at: :desc)
	end

	def new
		@book = Book.new
	end

	def create
		@book = Book.new(params_book)
		if @book.save
			redirect_to books_path
		else
			redirect_back new_book_path
		end
	end

	def edit
		
	end

	def update
		if @book.update(params_book)
			redirect_to books_path
		else
			redirect_back new_book_path
		end
	end

	private

	def params_book
		params.fetch(:book, {}).permit(:title, :description, :category)
	end

	def define_book
		@book = Book.find(params[:id])
	end
end

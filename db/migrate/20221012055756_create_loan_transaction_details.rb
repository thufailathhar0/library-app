class CreateLoanTransactionDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :loan_transaction_details do |t|
    	t.references :loan_transaction
    	t.references :book
      t.timestamps
    end
  end
end

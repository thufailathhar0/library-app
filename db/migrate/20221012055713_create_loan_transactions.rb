class CreateLoanTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :loan_transactions do |t|
    	t.datetime :due_date
    	t.boolean :is_return, default: false
    	t.references :user
      t.timestamps
    end
  end
end
